class Mult:
    def __init__(self):
        self._multIn1 = 0
        self._multIn0 = 0
        self._product = 0

    # Updates the sum value with current inputs
    def _mult(self):
        self._product = self._multIn1 * self._multIn0

    def _get_product(self):
        self._mult()
        return self._product

    def _set_mult(self, mult_in1, mult_in0):
        self._multIn1 = mult_in1
        self._multIn0 = mult_in0

    def __str__(self):
        self._mult()
        return '{} * {} = {}'.format(self._multIn1, self._multIn0, self.mult)

    mult = property(_get_product)