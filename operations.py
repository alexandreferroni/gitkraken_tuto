from add import *
from mult import *


class Operations(Add, Mult):
    def __init__(self):
        Add.__init__(self)
        Mult.__init__(self)

    def set_inputs(self, input1, input0):
        self._set_mult(input1, input0)
        # self._set_mult(input1, input0)

    def __str__(self):
        return '{}\n{}'.format(Add.__str__(self), Mult.__str__(self))