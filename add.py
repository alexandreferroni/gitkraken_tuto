class Add:
    def __init__(self):
        self._addIn1 = 0
        self._addIn0 = 0
        self._sum = 0

    # Updates the sum value with current inputs
    def _add(self):
        self._sum = self._addIn1 + self._addIn0

    def _get_sum(self):
        self._add()
        return self._sum

    def _set_add(self, add_in1, add_in0):
        self._addIn1 = add_in1
        self._addIn0 = add_in0

    def __str__(self):
        self._add()
        return '{} + {} = {}'.format(self._addIn1, self._addIn0, self.sum)

    sum = property(_get_sum)